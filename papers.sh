#!/bin/bash
# The shell script I used to get the data for my IWO project.
# Beschrijving:
#              Telt het aantal woorden per bestand.  
#              Telt per bestand hoevaak 'zo' voorkomt.
#              Telt per bestand hoevaak 'meer' voorkomt.
#              Telt per bestand hoevaak 'grote' voorkomt.
#              Telt per bestand hoevaak 'verkeerde' voorkomt.
#
# Gebruik: ./papers.sh 

echo "Telt het aantal woorden per bestand."
wc -w 6jan1861NRC.txt
wc -w 10jan1861NRC.txt
wc -w 1sep1895AH.txt
wc -w 3sep1895AH.txt
wc -w 3aug1990.txt
wc -w 18dec1990.txt
wc -w 1maart1994.txt
wc -w 4maart1994.txt

echo "Telt per bestand hoevaak 'zo' voorkomt."
echo "6jan1861NRC.txt"
grep -Eo '\w+' 6jan1861NRC.txt | grep '^zo$' | wc -l
echo "10jan1861NRC.txt"
grep -Eo '\w+' 10jan1861NRC.txt | grep '^zo$' | wc -l
echo "1sep1895AH.txt"
grep -Eo '\w+' 1sep1895AH.txt | grep '^zo$' | wc -l
echo "3sep1895AH.txt"
grep -Eo '\w+' 3sep1895AH.txt | grep '^zo$' | wc -l
echo "3aug1990.txt"
grep -Eo '\w+' 3aug1990.txt | grep '^zo$' | wc -l
echo "18dec1990.txt"
grep -Eo '\w+' 18dec1990.txt | grep '^zo$' | wc -l
echo "1maart1994.txt"
grep -Eo '\w+' 1maart1994.txt | grep '^zo$' | wc -l
echo "4maart1994.txt"
grep -Eo '\w+' 4maart1994.txt | grep '^zo$' | wc -l

echo "Telt per bestand hoevaak 'meer' voorkomt."
echo "6jan1861NRC.txt"
grep -Eo '\w+' 6jan1861NRC.txt | grep '^meer$' | wc -l
echo "10jan1861NRC.txt"
grep -Eo '\w+' 10jan1861NRC.txt | grep '^meer$' | wc -l
echo "1sep1895AH.txt"
grep -Eo '\w+' 1sep1895AH.txt | grep '^meer$' | wc -l
echo "3sep1895AH.txt"
grep -Eo '\w+' 3sep1895AH.txt | grep '^meer$' | wc -l
echo "3aug1990.txt"
grep -Eo '\w+' 3aug1990.txt | grep '^meer$' | wc -l
echo "18dec1990.txt"
grep -Eo '\w+' 18dec1990.txt | grep '^meer$' | wc -l
echo "1maart1994.txt"
grep -Eo '\w+' 1maart1994.txt | grep '^meer$' | wc -l
echo "4maart1994.txt"
grep -Eo '\w+' 4maart1994.txt | grep '^meer$' | wc -l

echo "Telt per bestand hoevaak 'grote' voorkomt."
echo "6jan1861NRC.txt"
grep -Eo '\w+' 6jan1861NRC.txt | grep '^grote$' | wc -l
echo "10jan1861NRC.txt"
grep -Eo '\w+' 10jan1861NRC.txt | grep '^grote$' | wc -l
echo "1sep1895AH.txt"
grep -Eo '\w+' 1sep1895AH.txt | grep '^grote$' | wc -l
echo "3sep1895AH.txt"
grep -Eo '\w+' 3sep1895AH.txt | grep '^grote$' | wc -l
echo "3aug1990.txt"
grep -Eo '\w+' 3aug1990.txt | grep '^grote$' | wc -l
echo "18dec1990.txt"
grep -Eo '\w+' 18dec1990.txt | grep '^grote$' | wc -l
echo "1maart1994.txt"
grep -Eo '\w+' 1maart1994.txt | grep '^grote$' | wc -l
echo "4maart1994.txt"
grep -Eo '\w+' 4maart1994.txt | grep '^grote$' | wc -l

echo "Telt per bestand hoevaak 'verkeerde' voorkomt."
echo "6jan1861NRC.txt"
grep -Eo '\w+' 6jan1861NRC.txt | grep '^verkeerde$' | wc -l
echo "10jan1861NRC.txt"
grep -Eo '\w+' 10jan1861NRC.txt | grep '^verkeerde$' | wc -l
echo "1sep1895AH.txt"
grep -Eo '\w+' 1sep1895AH.txt | grep '^verkeerde$' | wc -l
echo "3sep1895AH.txt"
grep -Eo '\w+' 3sep1895AH.txt | grep '^verkeerde$' | wc -l
echo "3aug1990.txt"
grep -Eo '\w+' 3aug1990.txt | grep '^verkeerde$' | wc -l
echo "18dec1990.txt"
grep -Eo '\w+' 18dec1990.txt | grep '^verkeerde$' | wc -l
echo "1maart1994.txt"
grep -Eo '\w+' 1maart1994.txt | grep '^verkeerde$' | wc -l
echo "4maart1994.txt"
grep -Eo '\w+' 4maart1994.txt | grep '^verkeerde$' | wc -l





