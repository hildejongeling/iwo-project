# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository is for the course Inleiding 'Wetenschappelijk Onderzoek' and stores my files for this course.

### How to use the shell script of my IWO project ###
First, clone the repository. Next you can go to the following sites and save the files as the name behind them:

[http://resolver.kb.nl/resolve?urn=KBNRC01:000030683:mpeg21:p001 ](Link URL)-> 1maart1994.txt
[http://resolver.kb.nl/resolve?urn=ddd:010161610:mpeg21:p001](Link URL) -> 1sep1895AH.txt
[http://resolver.kb.nl/resolve?urn=KBNRC01:000029934:mpeg21:p001](Link URL) -> 3aug1990.txt
[http://resolver.kb.nl/resolve?urn=KBNRC01:000029770:mpeg21:p001 ](Link URL) -> 18dec1990.txt
[http://resolver.kb.nl/resolve?urn=KBNRC01:000030743:mpeg21:p001 ](Link URL) -> 4maart1994.txt
[http://resolver.kb.nl/resolve?urn=ddd:010161613:mpeg21:p001](Link URL) -> 3sep1895AH.txt
[http://resolver.kb.nl/resolve?urn=ddd:000010006:mpeg21:p001 ](Link URL) -> 6jan1861NRC.txt
[http://resolver.kb.nl/resolve?urn=ddd:000010010:mpeg21:p001 ](Link URL) -> 10jan1861NRC.txt

Save them in the same directory as the shell script. Now you can run the shell script in the terminal.