#!/bin/bash
# A simple shell script example.
# Description: Counts the number of lines in a file.
#              Counts the number of unique words.      
#              Takes sentences that starts with 'de'.
#              Takes sentences that doesn't start with 'de'.
#
# Usage: ./sample-paper.sh FILE
#argument is the file, check if we get it.
TEXT=$1
if [ -z "$TEXT" ]
then 
    echo "Specify a file!"
    exit
fi

echo "Counts the number of lines in a file."
tr '\n' ' ' < $TEXT | sed 's/[.!?;,"]/ & /g' | sed 's/[.!?]/&\n/g' | wc -l

echo "Counts the number of unique words."
grep -Eo '\w+' $TEXT | sort | uniq -c | wc -l

echo "Takes sentences that starts with 'de'."
tr '\n' ' ' < $TEXT | sed 's/[.!?;,"]/ & /g' | sed 's/[.!?]/&\n/g' | grep 'De '

echo "Takes sentences that doesn't start with 'de'."
tr '\n' ' ' < $TEXT | sed 's/[.!?;,"]/ & /g' | sed 's/[.!?]/&\n/g' | grep -v 'De '
